<html>
    <head>
        <title>Liste des demandes</title>
        <link rel='stylesheet' type='text/css' href='css/tableau.css' media='all'>
       

    </head>
    <body>
        <div class="top">
            <div class="logo" style="float: left;">
                <img src="https://upload.wikimedia.org/wikipedia/fr/thumb/4/4a/Paris_Saint-Germain_Football_Club_%28logo%29.svg/768px-Paris_Saint-Germain_Football_Club_%28logo%29.svg.png" width="100" >
            </div>
            <div class="info">
                <span>Téléphone : 0669473925</span>
                <span>E-mail : memed95.ahmed95@gmail.com</span>
            </div>
        </div>
        <!--Ma liste de demandes -->
        <div class="Liste des demandes">
            <h1>Liste des demandes</h1>
            <form>
                <table class="tableau-style">

                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Date de naissance</th>
                            <th>Etat</th>
                         </tr>
                    </thead>


                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Boujemaoui</td>
                            <td>Ahmed</td>
                            <td>14/10/2003</td>
                            <td>Vérifié</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Lionel</td>
                            <td>Messi</td>
                            <td>24/06/1987</td>
                            <td>En attente</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Sofiane</td>
                            <td>Boufal</td>
                            <td>17/09/1993</td>
                            <td>Dossier incomplet</td>
                        </tr>
                        <tr>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                        <tr>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                    </tbody>
                   
                </table>
            </form>

        </div class="copyright">

        </div>

    </body>
</html>
# Etape 1

1/Quel est le masque de sous-réseau de chaque machine ?


**CIDR=16**
(M1) 192.168.10.10/16 


Le CIDR 11111111|11111111|00000000|00000000


masque sous réseau: 255.255.0.0


(M2) 192.168.0.10/16


Le CIDR 11111111|11111111|00000000|00000000


masque sous réseau: 255.255.0.0/16


(M3) 192.168.0.10/16


Le CIDR 11111111|11111111|00000000|00000000


masque sous réseau:255.255.0.0/16 


**CIDR=20**


(M1)192.168.10.10/20


11111111|11111111|11110000|00000000


masque sous réseau: 255.255.240.0/20


(M2)192.168.0.10/20


11111111|11111111|11110000|00000000


masque sous réseau: 255.255.240.0/20


(M3)192.168.0.10/20


11111111|11111111|11110000|00000000


masque sous réseau: 255.255.240.0/20

2/A quel sous-réseau IP appartient chaque machine ? Justifier.

(M1)=192.168.0.0
(M2)=192.168.0.0
(M3)=192.168.0.0

3/Peuvent-elles potentiellement communiquer entre elles ? Justifier. 

oui elles peuvent potentiellement communiquer entre elles car elles appartiennet au même réseau

4/Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.

 oui dans les conditions physiques elles  peuvent  communiqué entre elle si elles sont connecter à un même reseau ou à une switch 

5/Quelle commande sur le terminal Filius permet de voir la configuration IP d’une STA ?

La commande sur le terminal Filius permet de voir la configuration IP d’une STA est ipconfig

6/Quelle commande permet de tester que les messages IP sont bien transmis entre deux machines ?

La commande permet de tester que les messages IP sont bien transmis entre deux machines est  ping (ip adress) 

# Etape 2 

1/Quel média de communication a été ajouté ? Expliquer brièvement son fonctionnement.

Le  média de communication a été ajouté est un commutateurs (switch).Un commutateur réseau, est un équipement qui relie plusieurs segments dans un réseau informatique et de télécommunication et qui permet de créer des circuits virtuels. La commutation est un des deux modes de transport de trame au sein des réseaux informatiques et de communication, l'autre étant le routage.

2/Quel autre média aurait pu le remplacer (inexistant sur Filius) ? En quoi est-il différent du premier ?

L'autre média aurait pu le remplacer (inexistant sur Filius) est le hub qui est un Appareil relié à plusieurs machines en réseau, et permettant de concentrer les données pour les transmettre par un unique canal.

3/Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier.

Oui toutes les machines peuvent maintenant communiquer entre elles car elle sont communiqué qur le même réseau

4/Si nécessaire, corriger les configurations pour permettre à toutes les STAs de communiquer entre elles. Quelles sont les modifications apportées ?

il n'a pas besoin de modifier

5/Si M1 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse logique peut-elle utiliser ?
 
Si la machine 1 peut communiquer elle utilisera l'adresse de brodcast 192.168.255.255 

# Etape 3 

1/A quel réseau appartient la 4e machine (M4) ? Est-ce le même que celui des Machines M1, M2 et M3 ? Justifier.

La machine 4 n'appartient pas au même réseau car l'adresse réseau fini par 169 et pas par 168 comme sur les mzchine M1 M2 et M3

2/M4 peut-elle communiquer avec M1, M2 ou M3 ? Justifier.

Non elle ne peut pas communiquer car la M4 n'est pas sur le même réseau que la M1 M2 M3 


3/Si nécessaire, reconfigurer M2 pour lui permettre d’échanger des messages avec M4. Quels sont les changements nécessaires ?

il faut remplacer l'adresse reseau de la M2 qui est 198.168.0.10 par 192.169.0.11

4/Est-ce que M1 et M2 peuvent toujours communiquer ? Justifier.

Non la M1 et M2 ne peuvent pas communiquer car elle ne sont pas sur le même réseau  c'est la régle qui s'applique à tous 















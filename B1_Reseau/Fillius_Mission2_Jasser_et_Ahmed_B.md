# Mission 2: Simulation Filius d'échange entre deux sous-réseaux logiques
#Jasser Ahmed B
#Le Jeudi 21 octobre 2021

#Etape 1#

% Quel est le CIDR de chaque machine ? Justifier.

Le CIDR De la machine M1 Est de 16, grâce au 2 premiers octets sont allumé.
Le CIDR de la machine M2 est de 16, car les deux premiers octets sont de 255 grâce a cette information nous savons que 2 bits sont allumé
Le CIDR de la machine M3 est de 16, car les 2 premiers octets sont allumé 
Le CIDR de la machine M4 est de 16 car les 2 premiers octets sont de 255

% A quel sous-réseau IP appartient chacune de ces 4 machines ? Justifier.

Adresse IP M1:10.10.1.100
La machine 1 a pour sous-reseau ip 10.10.0.0 car le CIDR est de 16 donc les 2 premiers octets restent intactes et les 2 dernier sont éteint.
Adresse IP M2:10.0.1.255
La machine 2 a pour sous-reseau ip 10.0.0.0 car le CIDR est de 16 donc les 2 premier octets restent intactes et les 2 dernier sont éteint.
Adresse IP M3:10.0.10.245
La machine 3 a pour sous-reseau ip 10.0.0.0 car le CIDR est de 16 donc les 2 premier octets restent intactes et les 2 dernier sont éteint.
Adresse IP M4:10.10.20.128
La machine 4 a pour sous-reseau ip 10.10.0.0 car le CIDR est de 16 donc les 2 premier octets restent intactes et les 2 dernier sont éteint.



### Combien de machines maximums peuvent accueillir chacun des sous-réseaux identifiés ?

Ont peut connecter 65534 stas, car 2^(32-CIDR)-2= 65534

% Quelles machines peuvent potentiellement communiquer entre elles ? Justifier.

Les machines qui peuvent potentiellement communiquer entre elles sont 1 et 4 parceque elles appartiennent aux même réseaux(10.10.0.0) 

et 2,3 car elles appartiennent au même réseaux (10.0.0.0)


### Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.

Oui les machines 1 et 4 peuvent communiquer entre elles car nous avons effectuer un Ping ce qui nous a permit de vérifier leur compatibilité. Pour cela ont a effectuer la commande ping 10.10.20.128.
Oui les machines 2 et 3 peuvent communiquer entre elles car nous avons effectuer un Ping ce qui nous a permit de vérifier leur compatibilité. Pour cela ont a effectuer la commande ping 10.0.10.245.

### Quelle est la commande complète qui permet à M1 de tester la liaison avec M2

La commande compléte qui permet de tester la liaison avec la Machine M2 est ping 10.0.1.255

## Etape 2

###Quelles sont les adresses IP possibles pour M5 et M6 ?

Les adresse IP Possible pour la machine 5 sont toutes situées entre 10.10.0.1/16 et 10.10.255.254/16
Les Adresse IP Possible pour la machine 6 sont toutes situées entre 10.0.0.1/16 et 10.0.255.254/16

###Combien de machines peut-on encore ajouter dans chaque sous-réseau ?

Nous Pouvons Ajouter encore 65534 Stats possible au maximum  2^(32-CIDR)-2

###Si M6 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse IP peut-elle utiliser ?

Il faut utiliser l'adresse broadcast 10.0.255.255 de la machine 6 afin de communiquer avec toutes les machins de sous-réseau

###Quel média d'interconnexion est nécessaire pour permettre à toutes les machines d'échanger des messages ?





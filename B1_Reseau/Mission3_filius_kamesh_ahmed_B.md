# Mission 3 : Zoom sur les échanges entre machines
## MAVALY Kameshdassane ; BOUJEMAOUI Ahmed
## BTS 1
## 04/12/2021


### Etape 1

1) Quels sont les sous-réseaux logiques représentés dans ce réseau physique ? Justifier.


**Dans ce schéma il y a deux sous-reseaux logiques, le premier réseau logique comprend M1,M2 et M3 ont pour adresse 192.168.1.0/24 car l'adresse réseau est192.168.1.0/24.**
**Le premier réseau logique comprend M4,M5 et M6 ont pour adresse 172.12.0.0/16 car l'adresse réseau est 172.12.0.0/16.**


2) Compléter la configuration du routeur pour permettre à toutes les machines de communiquer entre-elles ? Spécifier toutes les étapes nécessaires.

**Pour que les deux-réseaux logiques communiquent entre elle il faut configurer les interfaces du routeur.**
**C'est a dire qu'il faut attribuer les adresses IP aux interfaces du routeur cette adresse IP va devenir la passerelle pour les ordinateurs.**
**Pour le premier réseau logique il faut renseigner l'adresse suivante: 192.168.0.255 et l'@ masque 255.255.255.0.**
**Pour le deuxième réseau logique il faut renseigner l'adresse suivante: 172.12.255.255 et l'@ masque 255.255.0.0.**
**Pour finir il faut renseigner les passerelles qui correspondent aux machines et effectuer les ping vers les autres machines**


3) Quelle commande est utilisée pour permettre de tester la bonne communication entre deux machines ?

**pour permettre de tester la connexion entre deux machines il faut utilisée la commande ping(et l'adresse IP de la machine à tester).**
**Par exemple pour faire communiquer la machine M1 et la M4 il faut utilisée la commande suivante depuis le M1: ping  172.12.0.2**


### Etape 2

1) Cliquer sur chaque commutateur (Switch R1 et Switch R2) pour faire apparaître leur table de correspondance. Quelles informations y trouve-t-on ?

**Quand t'on lance un ping d'une machine vers un autres  on voit le chemin du paquet.** 
**C'est a dire, si on lance un ping de la M1 vers le M4, on voit les chemins que les packets prennent et les ports par où elles passent.**


2) On choisit de faire comuniquer M1 et M2. Mais avant,-regarder la table des échanges de données et on la supprimera avec le menu contextuel "supprimer la table".
-opérer la commande *arp* dans la console de chaque machine. Que voit-on ?


**Avec la commande *arp* nous pouvons voir l'adresse IP et l'adresse MAC de la dernière machine qui à communiquer avec la machine.**


3)Opérer un ping à partir de M1 pour tenter de joindre M2.

-Quel chemin lumineux prend le message ?
-Voit-on des modifications dans la table de correspondance des Switch R1 et R2 ? Si Oui, lesquelles ?
-Afficher les échanges sur les machines M1 et M2 ? Quels types (protocole et couche) de messages ont été échangés ?
-opérer encore la commande arp dans la console de chaque machine. Que voit-on ?
-Que remarque-t-on si on échange un nouveau ping entre M1 et M2 ?


**La lumière commence par M1 passe par le switch R1 et fini sur M2**

**Non**

**Le protocole utilisé et *Icmp* ce protocole utilisent la couche réseau du modèle OSI**

**On voit l'adresse IP et l'adresse MAC de la dernière machine qui à communiquer avec la machine.**


-Recommencer les étapes 1 à 3, en choisissant de faire communiquer M3 et M6. Quels nouveaux changements voit-on ?

**On voit que la lumière passe par les switchs et le routeur. De plus on voit que dans la table de correspondance des switchs on ne voit pas tout le chemin parcouru par les paquets car le chemin emprunté deux Switch cela veut dire qu'il y a la moitié du chemin sur le switch R1 et l'autre moitié est sur le switch R2.**
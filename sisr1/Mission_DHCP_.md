# Mission : DHCP
## Boujemaoui Ahmed 
## 11/02/2022
## BTS 1

- Objectifs :

*Utilisation d'un service DHCP (configuration et mise en service)*
*Test du bon fonctionnement d'un service DHCP*
*Analyse des trames échangées lors des échanges avec le service DHCP*

*Cette mission nécessite l’utilisation du simulateur Filius*
*Le résultat des expérimentations sont à consigner dans un livrable à déposer sur Gitlab : (SISR1/Mission_DHCP.md)*

## Étape 2 

### 2.1 Quelle commande doit-on opérer pour vérifier ? 

La commande qu'on doit opéré pour vérifier la configuration est "ipconfig /all".

### 2.2 Quel résultat doit-on trouver ?
Le résultat que l'on doit trouver dans chacune des STAs est une adresse IP qui correspond a la plage d'adresse renseigner dans serveur DHCP.
La plage d'adresse est 192.168.1.1/24 à 192.168.1.11/24.


### 2.3  Quelle est la configuration de chacune des machines ? Ont-elles toutes la configuration attendue ? Si c'est le cas, peut-on poser un diagnostique ?

- Quelle est la configuration de chacune des machines ?


- M1 : @ IP = 192.168.1.3 ; Masque = 255.255.255.0 ; passerelle :  192.168.1.254/24  DNS : 192.168.2.1


- M2 : @ IP = 192.168.1.2 ; Masque = 255.255.255.0 ; passerelle :  192.168.1.254/24  ; DNS : 192.168.2.1/24


- M3 : @ IP = 192.168.1.4 ; Masque = 255.255.255.0 ; passerelle :  192.168.1.254/24  ; DNS : 192.168.2.1/24

- Ont-elles toutes la configuration attendue ?
oui toutes les STAs dans le réseaux 192.168.1.0/24 ont toutes les configuration attendue.


### 3. Diffèrentes requêtes et réponses envoyées et reçu :


- Les STAs envoie une premère requête cette étape s'appelle le "DHCP Discovery" elle permet de demander une configuration IP
- Aprés la réception du requête le serveur répond aux STAs cette étapes permet de proposer des configuration IP aux machines "DHCP Offer"
- Par la suite les machine envoient une requête vers le serveur cette étapes s'appelle "DHCP REQUEST" elle permet de voir si les machine on accepter la configuration IP
- Enfin, le serveur répond si oui ou non la permission est accorder.

### Est-ce que le déroulement est similaires entre les différentes STAs ?
Le déroulement n'est pas similaires entre les différentes STAs car toutes les demandes son formés de la même manières.

## Etape 3 

### 1. Quelle information est primordiale pour identifier une machine sur le réseau ?
 L'adresse MAC est primordiale car elle nous servira a identifier l'appareil dans le réseau. 

### 2. 

### 3. Quels types de messages sont éhangés entre les STAs et Le serveur DHCP ? Identifier les messages et leur contenu.

Les messages qui sont échangés entre la machine et le serveur DHCP est le protocle DHCP ce message est constituer des requêtes et de réponses 

### 4. Activer le serveur DNS pour lui permettre de remplir sa principale fonction. Tester le bon fonctionnement de la configuration réseau pour la valider (Noter la procédure et résultats attendus).

Le Service DNS fonctionne pas correctement car quand t-on saisi le nom de domaine de la machine M1 sur l'un des STAs du réseau logique 172.12.1.0/16 cela ne fonction car la machine M1 a changer d'adresse IP mais si on fait recherhe le nom de domaine de la machine M6 cela fonctionne car l'adresse IP n'a pas changer.

# Mission 1 : Zoom sur les échanges entre machines (Exploration ARP et DNS)
## Boujemaoui Ahmed
## BTS 1
## 20/01/2021


### Etape 1

1) Quels sont les sous-réseaux logiques représentés dans ce réseau physique ? Justifier.

**Dans ce réseau physique on distingue deux sous-réseaux logiques, le premier réseau logique comprend M1,M2 et M3, Ces machines ont des adresses qui commencent par 192.168.1.0/24 car l'adresse réseau est**
**192.168.1.0/24 et le deuxième réseau logique comprend M4,M5 et M6 qui ont pour adresse 172.12.0.0/16 car l'adresse réseau est 172.12.0.0/16.**


2) Compléter la configuration du routeur pour permettre à toutes les machines de communiquer entre-elles ? Spécifier toutes les étapes nécessaires.

**Pour que les deux-réseaux logiques communiquent entre elle il faut configurer les interfaces du routeur c'est a dire qu'il faut configurée la passerelle. Pour configurer cette passerelle il faut attribuer les adresses IP aux interfaces du routeur cette dernière va devenir la passerelle pour les ordinateurs.**
**Pour le premier réseau logique il faut renseigner l'adresse suivante: 192.168.0.255 et l'@ masque 255.255.255.0.**
**Pour le deuxième réseau logique il faut renseigner l'adresse suivante: 172.12.255.255 et l'@ masque 255.255.0.0.**
**Pour finir il faut renseigner les adresses de la passerelle correspondant à chaque ordinateur et effectuer les pings vers les autres machines**


3) Quelle commande est utilisée pour permettre de tester la bonne communication entre deux machines ?

**pour permettre de tester la connexion entre deux machines il faut utilisée la commande ping(et l'adresse IP de la machine à tester).**
**Par exemple pour faire communiquer la machine M1 et la M4 il faut utilisée la commande suivante depuis la machine M1: ping  172.12.0.2**


### Etape 2

1) Cliquer sur chaque commutateur (Switch R1 et Switch R2) pour faire apparaître leur table de correspondance. Quelles informations y trouve-t-on ?

**Quand t'on lance un ping d'une machine vers un autre on voit les chemins emprunter par les paquets.** 
**C'est a dire, si on lance un ping de la machine M1 vers la machine M4, on voit les chemins que les paquets prennent et les ports par où elles passent.**


2) On choisit de faire comuniquer M1 et M2. Mais avant,-regarder la table des échanges de données et on la supprimera avec le menu contextuel "supprimer la table".
-opérer la commande *arp* dans la console de chaque machine. Que voit-on ?


**Avec la commande *arp* nous pouvons voir l'adresse IP et l'adresse MAC de la dernière machine qui à communiquer avec la machine.**


3)Opérer un ping à partir de M1 pour tenter de joindre M2.

-Quel chemin lumineux prend le message ?


**La lumière commence par M1 passe par le switch R1 et fini sur M2**


-Voit-on des modifications dans la table de correspondance des Switch R1 et R2 ? Si Oui, lesquelles ?


**Non**


-Afficher les échanges sur les machines M1 et M2 ? Quels types (protocole et couche) de messages ont été échangés ?


**Le protocole utilisé et *Icmp* ce protocole utilisent la couche réseau du modèle OSI**


-opérer encore la commande arp dans la console de chaque machine. Que voit-on ?


**On voit l'adresse IP et l'adresse MAC de la dernière machine qui à communiquer avec la machine.**


-Que remarque-t-on si on échange un nouveau ping entre M1 et M2 ?



-Recommencer les étapes 1 à 3, en choisissant de faire communiquer M3 et M6. Quels nouveaux changements voit-on ?

**On voit que la lumière passe par les switchs et le routeur. De plus on voit que dans la table de correspondance des switchs on ne voit pas tout le chemin parcouru par les paquets car le chemin emprunté deux Switch cela veut dire qu'il y a la moitié du chemin sur le switch R1 et l'autre moitié est sur le switch R2.**

### Etape 3

1) A quoi sert le serveur DNS dans un réseau physique ?

**le serveur DNS dans un réseau logique sert à maintenir son annuaire d'adresse IP à jour de plus on peut mettre des restrictions afin de pouvoir sécuriser l'entreprise**


2) Les machines M1 et M6 sont utiles à tous les utilisateurs du réseau physique et on aimerait leur permettre d'y accéder de manière plus aisée. Comment le serveur DNS y contribuera ?


**pour que les machines M1 et M6 puissent être accessible aux autres machines il faut renseigner les adresses IP des machines M1 et M6 dans l'annuaire du serveur DNS.**



3) Ajouter une Machine de type tour qu'on appellera 'Serveur DNS'.

-On la connectera à la troisième carte réseau du routeur.

-Quelle adresse logique lui donner ?

**comme c'est un réseau on peut lui donner une nouvelle plage d'adresses:**
**on peut lui attribuer l'adresse 192.168.2.1**

-Compléter la  configuration de cette nouvelle machine pour lui permettre de communiquer avec toutes les machines du réseau physique et vice-vera.

-Y installer un serveur DNS via l'utilitaire d'installation logiciel de Filius.
-Configurer le serveur pour donner un nom particulier à M1 et M6. Noter les étapes.

4) Est-il possible d'accéder à M1 et M6 via leur nouveau nom ? Sinon, quelles configurations supplémentaires sont nécessaires pour y parvenir ?

**il n'est pas encore possible de joindre à M1 et M6 car il faut renseigner l'adresse IP du serveur DNS sur les Machines M2,M3,M4 et M5.**


5) Analyser les trames/paquets échangés lors de la mise en communication de M5 et M1 par exemple. Voit-on des nouveaux types de paquets ? et entre quelles machines sont-ils échangés ?


**Oui, on voie des nouveaux types de paquets. Elle communique tout d'abord avec le serveur DNS et ensuite le Serveur DNS envoie la machine M5 vers la machine M1**
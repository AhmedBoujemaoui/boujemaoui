# Procédure d’installation DEBIAN 11 par la fonction PXE
## MAVALY Kameshdassane, Boujemaoui Ahmed
## BTS 1


### LENEVO ThinkCentre 
### Model : MT-M 3167 – B28
### Numéro de S/N : PBKAX39



## Etape 1 : 

#### Tout d’abord, il faut vérifier l’ordinateur :

#### -	S’il y a internet 
#### -	Prends en charge le système 64bits  
#### -	Vérifier la version de l’OS déjà installer 



## ETAPE 2 : 
#### Après avoir vérifié l’ordinateur redémarrer la machine et appuyer sur la touche [F12] pour accéder au BOOT MENU et choisir l’option [network1 : Realtek PXE BO2 D00].

![config 1](img/1.JPEG) 


## ETAPE 3 : 

#### Après avoir choisi l’option PXE attendre que la fenêtre ci-dessous s’affiche et en fonction du système prit en charge, choisir la version 64 ou 32 bits du système d’exploitation.Pour l’ordinateur LENEVO ThinkCenter/ MT-M 3167 – b28 choisissait l’option suivante et appuyé sur la touche [entrée].

![config 1](img/2.JPEG)

## ETAPE 4 :
#### Après avoir choisi la version laisser faire la détection de la carte réseau et les chargements des composants.

![config 1](img/3.JPEG)

## ETAPE 5 :
#### Une fois arriver sur cette fenêtre appuyer sur la touche [entrée] pour continuer.
![config 1](img/4.JPEG)

## ETAPE 6 :
#### Une fois arriver sur la page ci-dessous :

#### - A l'aide de la touche [Espace] sélectionner [l’option Station de travail mobile].
#### - Après avoir choisi l’option appuyer sur le bouton [continuer].
![config 1](img/5.JPEG)

## ETAPE 7 :
####Laisser les différentes étapes et l’installation s’effectuer et appuyer sur le bouton [continuer] avec la touche [entrée].

![config 1](img/6.JPEG)

## ETAPE 8 :
#### Laisser finaliser l’installation cela prendra un instant.

![config 1](img/7.JPEG)

## ETAPE 9 :

#### Une fois que l’installation est terminée [appuyer] sur la touche [entrée] :

![config 1](img/8.JPEG)

# ETAPE 10 : 
#### Se connecter à l’aide de ses identifiants et profiter de DEBIAN 11.

![config 1](img/9.JPEG)
